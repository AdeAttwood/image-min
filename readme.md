# Image Min

~~~ bash
 ___                              __  __ _       
 |_ _|_ __ ___   __ _  __ _  ___  |  \/  (_)_ __  
  | || '_ ` _ \ / _` |/ _` |/ _ \ | |\/| | | '_ \ 
  | || | | | | | (_| | (_| |  __/ | |  | | | | | |
 |___|_| |_| |_|\__,_|\__, |\___| |_|  |_|_|_| |_|
                      |___/                                            
Minify png and jpg images to reduce disk space.

Usage: $0 [options]

OPTIONS:
 -h                    Show this message
 -V                    Print the version number
 -s --src     <dir>    The directory of the images to minify
 -q --quality [number] Set maximum image quality factor. Valid quality values: 0 - 100
 -c --cache   [dir]    The directory to look for the cache files [default: $CACHE]
 --no-cache

EXAMPLES:
 $0 -s my/dir
 $0 -s my/dir -q 40 --no-cache
 $0 -s my/dir -q 40 -c new/cache/dir
~~~

## Dependencies

Image min depends on three packages two for minifying pngs and one for the jpegs.

- [jpegoptim](https://github.com/tjko/jpegoptim)
- [optipng](http://optipng.sourceforge.net/)
- [pngquant](https://pngquant.org/)

## Installation

This package depends on 3 image minification programs they will all be installed with the installer. The install takes one argument that is the package manager you are using. There are two options `apt` and `yum`.

### Example installing with apt

~~~bash
 curl https://gitlab.com/AdeAttwood/image-min/raw/master/install.bash | bash -s apt
~~~

### Example installing with yum
~~~bash
 curl https://gitlab.com/AdeAttwood/image-min/raw/master/install.bash | bash -s yum
~~~

