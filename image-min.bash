#! /usr/bin/env bash
#=============================================================================== {{{
#
#          FILE: image-min.bash
#
#         USAGE: image-min -s [dir]
#
#   DESCRIPTION: Minify png and jpg images to reduce disk space.
#
#  REQUIREMENTS: pngquant optipng jpegoptim
#        AUTHOR: Ade Attwood <ade@practically.io>
#          LINK: https://gitlab.com/AdeAttwood/image-min
#       VERSION: 1.0.1
#       CREATED: Sat  4 Nov 15:52:49 UTC 2017
#
#=============================================================================== }}}

# GLOBAL VARIABLES {{{

APP_VERSION="v1.0.1"
CURRENT_DIRECTORY=$(pwd);

# }}}

# OPTION DEFAULTS {{{

QUALITY=50;
CACHE="$HOME/.config/image-min";

# }}}

# FUNCTIONS {{{

#    FUNCTION: help  {{{
# DESCRIPTION: Prints the help message
#     RETURNS: string
function help {
cat <<-END
  ___                              __  __ _       
 |_ _|_ __ ___   __ _  __ _  ___  |  \/  (_)_ __  
  | || '_ \` _ \ / _\` |/ _\` |/ _ \ | |\/| | | '_ \ 
  | || | | | | | (_| | (_| |  __/ | |  | | | | | |
 |___|_| |_| |_|\__,_|\__, |\___| |_|  |_|_|_| |_|
                      |___/                       
Minify png and jpg images to reduce disk space.

Usage: $0 [options]

OPTIONS:
 -h                    Show this message
 -V                    Print the version number
 -s --src     <dir>    The directory of the images to minify
 -q --quality [number] Set maximum image quality factor. Valid quality values: 0 - 100
 -c --cache   [dir]    The directory to look for the cache files [default: $CACHE]
 --no-cache

EXAMPLES:
 $0 -s my/dir
 $0 -s my/dir -q 40 --no-cache
 $0 -s my/dir -q 40 -c new/cache/dir
END
} # }}}

#    FUNCTION: minifyFile  {{{
# DESCRIPTION: Minifies a single image
#  PARAMETERS: [file]
#     RETURNS: boolean
function minifyFile {
    local _FILE=$@;
    local _TYPE=$(file --mime-type "$_FILE"  | awk '{print $NF}');
    local _FULL_PATH=$(readlink -f "$_FILE");

    if [ "$CACHE" != "0" ] && [ "$(isMini "$_FULL_PATH")" == "1" ]; then
        return 0;
    fi

    if [ "$_TYPE" == "image/png" ]; then
        pngquant --quality=$QUALITY --verbose --force --ext ".png" "$_FILE";
        optipng -o3 -force -strip "all" "$_FILE";
    elif [ "$_TYPE" == "image/jpeg" ]; then
        jpegoptim -m$QUALITY  -T10 -o --strip-all "$_FILE";
    else
        echo "Invalid mime type \"$_TYPE\"";
        return 0;
    fi

    return 1;
} # }}}

#    FUNCTION: minifyDir  {{{
# DESCRIPTION: Minifies all images in a directory
#  PARAMETERS: [dir]
#     RETURNS: void
function minifyDir {
    find $SRC -type f -print0 | while IFS= read -r -d '' _FILE; do
        minifyFile $_FILE;
    done
} # }}}

#    FUNCTION: isMini  {{{
# DESCRIPTION: Checks to see if a file has already been minified.
#  PARAMETERS: [dir]
#     RETURNS: boolean
function isMini {
    if [ ! -d "$CACHE" ]; then
        mkdir -p "$CACHE";
        touch "$CACHE/1.cache";
    fi

    local _IF_MINI=$(grep "$1" "$CACHE/1.cache");

    if [ ! -z $_IF_MINI ]; then
        echo 1;
    else
        echo $1 >> "$CACHE/1.cache";
        echo 0;
    fi
} # }}}

# }}}

# PARSE ARGUMENTS {{{
TEMP=`getopt -o hVs:q:c: --long src:,quality:cache:,no-cache  -- "$@"`
eval set -- "$TEMP"
while true ; do
    case "$1" in
        -s|--src)
            case "$2" in
                *) SRC=$2 ;;
            esac
            shift 2
            ;;
        -q|--quality)
            QUALITY=$2;
            shift 2
            ;;
        -c|--cache)
            CACHE=$2;
            shift 2
            ;;
        --no-cache)
            CACHE=0;
            shift
            ;;
        -h)
            help
            exit 0
            shift
            ;;
        -V)
            echo "$APP_VERSION"
            exit 0
            shift
            ;;
        --)
            shift
            break
            ;;
	    *)
		   echo "Invalid option $1";
           help
           exit 1
           ;;
    esac
done
# }}}

# VALIDATE ARGUMENTS {{{

if [ -z "$SRC" ]; then # {{{
	help;
	exit 1;
fi # }}}

# }}}

# MAIN {{{

echo "Minifying images at $QUALITY%";

if [ -d $SRC ]; then # {{{
	minifyDir;
elif [ -f $SRC ]; then
    minifyFile $SRC;
else
    echo "Invalid destenation";
    exit 1;
fi # }}}

# }}}

exit 0;
