#!/usr/bin/env bash

# GLOBAL VARS # {{{

CURRENT_DIRECTORY=$( pwd );

# }}}

function help { # {{{
cat <<-END
Usage: $0 <package manager>

Install image-min and its dependencies

Avalible package managers:
    - apt
    - yum
END
} # }}}

function aptInstal { # {{{
    sudo apt-get install -y jpegoptim optipng pngquant;
} # }}}

function yumInstal { # {{{
    sudo yum install -y epel-release
    sudo yum install -y pngquant jpegoptim optipng;
} # }}}

function installSctrpt { # {{{
    curl https://gitlab.com/AdeAttwood/image-min/raw/master/image-min.bash > image-min.bash
    chmod +x image-min.bash
    sudo mv image-min.bash /usr/local/bin/image-min
} # }}}

if [ ! -z $1 ]; then # {{{
    case "$1" in
        "apt")
            aptInstal;
            installSctrpt;
            ;;
        "yum")
            yumInstal;
            installSctrpt;
            ;;
	    *)
		   echo "Invalid package manager $1";
           echo "";
           help
           exit 1
           ;;
    esac
    AFTER=$(date "+%Y-%m-%d");
else
    help;
    exit 1;
fi # }}}